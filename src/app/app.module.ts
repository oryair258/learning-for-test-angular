import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import {RouterModule,Routes} from '@angular/router';
import { UserComponent } from './user/user.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UsersService } from './users/users.service';
import { PostFormComponent } from './post-form/post-form.component';
import { UserFormComponent } from './user-form/user-form.component';

export const firebaseconfig = {
    apiKey: "AIzaSyDkdaIbCt_Qrf05uggBZV2Cqna6BDwEx5c",
    authDomain: "learning-angular-abbdb.firebaseapp.com",
    databaseURL: "https://learning-angular-abbdb.firebaseio.com",
    storageBucket: "learning-angular-abbdb.appspot.com",
    messagingSenderId: "195118189070"
  }

const appRoutes:Routes = [
{path:'users', component: UsersComponent},
{path:'posts', component: PostsComponent},
{path:'', component: UsersComponent},
{path:'**', component: PageNotFoundComponent},
]



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    UserComponent,
    PageNotFoundComponent,
    PostFormComponent,
    UserFormComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
	  RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseconfig)
  ],
  providers: [PostsService,UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
